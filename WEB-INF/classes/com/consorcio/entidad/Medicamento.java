package com.consorcio.entidad;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
@Table(name="tb_medicamento")
public class Medicamento implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_med")
	private int codMedicamento;
	@Column(name="nom_med")
	private String nomMedicamento;
	@Column(name="stock_med")
	private int stockMedicamento;
	@Column(name="pre_med")
	private double precioMedicamento;
	@Temporal(TemporalType.DATE)
	@Column(name="fec_ven_med")
	private Date fechaVencimiento;//Date paquete java.util
	//relaci�n uno a muchos 
	@OneToMany(mappedBy="medicamento")
	private List<MedicamentoHasBoleta>
					listaMedicamentoHasBoleta;
	

	public int getCodMedicamento() {
		return codMedicamento;
	}
	public void setCodMedicamento(int codMedicamento) {
		this.codMedicamento = codMedicamento;
	}
	public String getNomMedicamento() {
		return nomMedicamento;
	}
	public void setNomMedicamento(String nomMedicamento) {
		this.nomMedicamento = nomMedicamento;
	}
	public int getStockMedicamento() {
		return stockMedicamento;
	}
	public void setStockMedicamento(int stockMedicamento) {
		this.stockMedicamento = stockMedicamento;
	}
	public double getPrecioMedicamento() {
		return precioMedicamento;
	}
	public void setPrecioMedicamento(double precioMedicamento) {
		this.precioMedicamento = precioMedicamento;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public List<MedicamentoHasBoleta> getListaMedicamentoHasBoleta() {
		return listaMedicamentoHasBoleta;
	}
	public void setListaMedicamentoHasBoleta(List<MedicamentoHasBoleta> listaMedicamentoHasBoleta) {
		this.listaMedicamentoHasBoleta = listaMedicamentoHasBoleta;
	}
	
}






