package com.consorcio.action;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.api.UIData;

import com.consorcio.entidad.Boleta;
import com.consorcio.entidad.Cliente;
import com.consorcio.entidad.Detalle;
import com.consorcio.entidad.Medicamento;
import com.consorcio.entidad.MedicamentoHasBoleta;
import com.consorcio.modelo.modeloBoleta;
import com.consorcio.modelo.modeloCliente;
import com.consorcio.modelo.modeloMedicamento;

import utils.Constantes;
@ManagedBean(name="ventas")
@ViewScoped
public class ventaAction {
	//Cliente
	private String apellido;
	private List<Cliente> listaCliente;
	private Cliente cliente;
	private UIData dtFilaCliente;
	//medicamento
	private String nombre;
	private List<Medicamento> listaMedicamento;
	private Medicamento medicamento;
	private UIData dtFilaMedicamento;
	//ventas
	//sesi�n de tipo Map
	private Map<String, Object> sesion=FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap();
	private String cantidad;
	
	public void consultaClienteXApellido(){
		listaCliente=new modeloCliente().listaCliente(apellido);
	}
	public void seleccionarCliente(){
		cliente=(Cliente) dtFilaCliente.getRowData();
	}
	public void consultaMedicamentoXNombre(){
		listaMedicamento=new modeloMedicamento().listaXnombre(nombre);
	}
	public void seleccionarMedicamento(){
		medicamento=(Medicamento) dtFilaMedicamento.getRowData();
	}
	public void adicionar(){
		//recuperar la clave "detalle"
		List<Detalle>lista=(ArrayList<Detalle>)sesion.get("detalle");
		//validar
		if(lista==null)
			lista=new ArrayList<Detalle>();
		
		//objeto de la clase Detalle
		Detalle d=new Detalle();
		d.setCodMed(medicamento.getCodMedicamento());
		d.setNomMed(medicamento.getNomMedicamento());
		d.setCantidad(Integer.parseInt(cantidad));
		d.setPrecio(medicamento.getPrecioMedicamento());
		//enviar "d" a lista 
		lista.add(d);
		//crear o actualizar la clave "detalle"
		sesion.put("detalle", lista);
	}
	public double getMonto(){
		List<Detalle>lista=(ArrayList<Detalle>)sesion.get("detalle");
		double s=0;
		if(lista!=null){
			for(int i=0;i<lista.size();i++)
				s+=lista.get(i).getImporte();
		}
		return s;
	}
	public void registraVenta() {
		try {
			Boleta b = new Boleta();
			Cliente c = new Cliente();
			c.setCodCliente(cliente.getCodCliente());
			b.setClienteBoleta(c);
			b.setFechaEmision(new Date());
			b.setMonto(getMonto());
			//recuperar sesion
			//recuperar detalle de la venta
			List<Detalle> boleta = (ArrayList<Detalle>) sesion.get("detalle");
			if (boleta != null) {
				for (Detalle s : boleta) {
					MedicamentoHasBoleta mhb = new MedicamentoHasBoleta();
					Medicamento p = new Medicamento();
					p.setCodMedicamento(s.getCodMed());
					mhb.setCantidad(s.getCantidad());
					mhb.setPrecio(s.getPrecio());
					mhb.setMedicamento(p);
					b.addMedicamentoHasBoleta(mhb);
				}
				modeloBoleta m = new modeloBoleta();
				m.registraBoleta(b);
				boleta.clear();
				sesion.put("detalle",boleta);
				cliente=new Cliente();
				Constantes.mensaje("Sistema", "Registro correcto", FacesMessage.SEVERITY_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public List<Cliente> getListaCliente() {
		return listaCliente;
	}
	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public UIData getDtFilaCliente() {
		return dtFilaCliente;
	}
	public void setDtFilaCliente(UIData dtFilaCliente) {
		this.dtFilaCliente = dtFilaCliente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Medicamento> getListaMedicamento() {
		return listaMedicamento;
	}
	public void setListaMedicamento(List<Medicamento> listaMedicamento) {
		this.listaMedicamento = listaMedicamento;
	}
	public Medicamento getMedicamento() {
		return medicamento;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	public UIData getDtFilaMedicamento() {
		return dtFilaMedicamento;
	}
	public void setDtFilaMedicamento(UIData dtFilaMedicamento) {
		this.dtFilaMedicamento = dtFilaMedicamento;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	
}



