package com.consorcio.modelo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import com.consorcio.entidad.Cliente;
public class modeloCliente {
	//objeto para obtener la unidad de persistencia que se encuentra
	//en el archivo persistence.xml
	EntityManagerFactory fabrica=Persistence.
			createEntityManagerFactory("PE");
	
	public List<Cliente> listaCliente(String ape){
		EntityManager manager=fabrica.createEntityManager();
		List<Cliente> data=null;
		TypedQuery<Cliente> resul=null;
		try {
			String hql="select c from Cliente c where c.apeCliente like ?1";
			resul=manager.createQuery(hql,Cliente.class);
			resul.setParameter(1, ape+"%");
			data=resul.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			manager.close();
			fabrica.close();
		}
		return data;
	}
	
}
