package com.consorcio.modelo;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.consorcio.entidad.Medicamento;
public class modeloMedicamento {
	EntityManagerFactory fabrica=Persistence.
			createEntityManagerFactory("PE");

	//listar todos los medicamentos "hql"
	public List<Medicamento> lista(){
		EntityManager manager=fabrica.createEntityManager();
		List<Medicamento> data=null;
		TypedQuery<Medicamento> resul=null;
		try {
			String hql="select m from Medicamento m";
			resul=manager.createQuery(hql,Medicamento.class);
			data=resul.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			manager.close();
			fabrica.close();
		}
		return data;
	}
	//listar todos los medicamentos que inicien con la letra a
	public List<Medicamento> listaXnombre(String nom){
		EntityManager manager=fabrica.createEntityManager();
		List<Medicamento> data=null;
		TypedQuery<Medicamento> resul=null;
		try {
			String hql="select m from Medicamento m where "+
						"m.nomMedicamento like concat(:param,'%')";
			resul=manager.createQuery(hql,Medicamento.class);
			resul.setParameter("param", nom);
			data=resul.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			manager.close();
			fabrica.close();
		}
		return data;
	}
				
				
}

