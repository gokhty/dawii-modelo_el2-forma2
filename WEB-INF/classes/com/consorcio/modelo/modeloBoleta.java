package com.consorcio.modelo;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.consorcio.entidad.Boleta;
import com.consorcio.entidad.MedicamentoHasBoleta;
import com.consorcio.entidad.MedicamentoHasBoletaPK;
public class modeloBoleta {
	EntityManagerFactory fabrica=Persistence.
				createEntityManagerFactory("PE");
	public void registraBoleta(Boleta bol){
		EntityManager manager=fabrica.
					createEntityManager();
		try {
			manager.getTransaction().begin();
			//grabar cabacera
			manager.persist(bol);
			manager.flush(); 
			//grabar detalle
			for(MedicamentoHasBoleta mhb: bol.getListaHasBoleta()){
				manager.persist(bol);
				manager.flush(); 
				MedicamentoHasBoletaPK mhbPK=new MedicamentoHasBoletaPK();
				mhbPK.setNumBoleta(bol.getNumBoleta());
				mhbPK.setCodMedicamento(mhb.getMedicamento().getCodMedicamento() );
				mhb.setPk(mhbPK);
				manager.persist(mhb);
				manager.flush();
			}
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
		}
		finally {
			manager.close();
			fabrica.close();
		}
	}
	
	
}
